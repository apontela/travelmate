<?php get_header() ?>


<div class="blog-header" style="background-image: url(<?php the_field("hero_image_blog", get_option('page_for_posts')) ?>)">
<?php get_template_part(navbar) ?>
    <h1><?php the_field('introduccion_de_blog', get_option('page_for_posts')) ?></h1>
</div>
<div class="blog-posts__grid">
<?php if (have_posts()) : ?>
      <?php while (have_posts()) : the_post(); ?>
        <div>
        <a href="<?php the_permalink() ?>"><h2><?php the_title() ?></h2></a>
        <p><?php the_excerpt() ?></p>
        </div>
      <?php  endwhile; 
    endif; ?>
</div>    
<?php get_sidebar() ?>
<?php get_footer() ?>