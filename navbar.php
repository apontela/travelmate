<nav>
    <div class="nav__first"><a href="http://localhost:8888/wordpress/"><img src="<?php the_field("nav-logo", get_option('page_for_posts')) ?>" alt="logo" ></a></div>
    <div class="nav__second"><a href="http://localhost:8888/wordpress/#herramientas"><i class="fas fa-toolbox"></i> <span>herramientas</span></a></div>
    <div class="nav__third"><a href="http://localhost:8888/wordpress/#footer"><i class="fas fa-inbox"></i> <span>contacto</span></a></div>
    <div class="nav__fourth"><a href="http://localhost:8888/wordpress/blog/"><i class="fas fa-book-open"></i> <span>blog</span></a></div>
    <div class="nav__fifth"><a href="http://localhost:8888/wordpress/diario/"><i class="fas fa-book"></i> <span>diario</span></a></div>
    <label for="toggle">&#9776;</label>
    <input type="checkbox" id="toggle"/>
    <div class="nav__adaptative">
        <a href="http://localhost:8888/wordpress/#herramientas"><i class="fas fa-toolbox"></i> <span>herramientas</span></a>
        <a href="http://localhost:8888/wordpress/#footer"><i class="fas fa-inbox"></i> <span>contacto</span></a>
        <a href="http://localhost:8888/wordpress/blog/"><i class="fas fa-book-open"></i> <span>blog</span></a>
        <a href="http://localhost:8888/wordpress/diario/"><i class="fas fa-book"></i> <span>diario</span></a>
    </div>

</nav>
