<?php

    /*
        Template Name: Colorful Maps
    */

?>

<?php get_header() ?>
<div class="all">
<div class="maps-body2">
    <?php get_template_part(navbar) ?>
    <h1><?php the_title() ?></h1>
</div>
<div class="container"><div id="map"></div></div>
<script>
        let coord = {
        santiago: {lat:-33.459229,lng:-70.645348},
        caracas: {lat:10.500000,lng:-66.916664},
        buenos_aires: {lat:-34.603722,lng:-58.381592},
        lima: {lat:-12.046374,lng:-77.042793},
        washington: {lat:47.751076,lng:-120.740135},
        madrid: {lat:40.416775,lng:-3.703790},
        medellin: {lat:6.25184,lng:-75.56359},
        rio: {lat:-22.9032,lng: -43.1729},
        ciudad_de_mexico: {lat:19.432608,lng:-99.133209},
        toronto: {lat:43.653908,lng:-79.384293},
    };
    function initMap() {
        let options = {
            zoom:11,
            center:{lat:-33.459229,lng:-70.645348}
        }

        let map = new google.maps.Map(document.getElementById('map'), options);

        let marker = new google.maps.Marker({
            position:{lat:-33.3593085,lng:-70.7265231},
            map:map,
        });

        let infoWindow = new google.maps.InfoWindow ({
            content: '<h4>Home</h4>'
        });
        marker.addListener('click', function() {
            infoWindow.open(map,marker);
        })
}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCaUP4Oc_2Y8djr5Lt3q2e7I8kjsDPRa6A&callback=initMap"></script>
</div>

<?php get_sidebar() ?>
<?php get_footer() ?>