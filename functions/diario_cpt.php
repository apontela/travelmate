<?php
function create_post_type() {
  register_post_type( 'diario',
    array(
      'labels' => array(
        'name' => __( 'Aventuras' ),
        'singular_name' => __( 'Aventura' )
      ),
      'public' => true,
      'has_archive' => true,
      'supports' => array( 'title', 'editor', 'custom-fields','thumbnail' ),
      'menu_icon'           => 'dashicons-book-alt',
    )
  );
}
add_action( 'init', 'create_post_type' );
?>