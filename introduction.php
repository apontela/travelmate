<div class="introduction">
    <div class="introduction__steps row">
        <div class="introduction__steps__first col-sm-12 col-md-6 col-lg-4 col-xl-4">
            <img src="<?php the_field("introduction__steps__first") ?>" alt="icono">
            <p><?php the_field('introduction__steps__first-text') ?></p>
        </div>
        <div class="introduction__steps__second col-sm-12 col-md-6 col-lg-4 col-xl-4">
            <img src="<?php the_field("introduction__steps__second") ?>" alt="icono">
            <p><?php the_field('introduction__steps__second-text') ?></p>
        </div>
        <div class="introduction__steps__third col-sm-12 col-md-4 col-lg-4 col-xl-4">
            <img src="<?php the_field("introduction__steps__third") ?>" alt="icono">
            <p><?php the_field('introduction__steps__third-text') ?></p>
        </div>
    </div>
    <div class="introduction__comment row">
        <div class="introduction__comment__image col-sm-6 col-md-6 col-lg-6 col-xl-6" style="background-image: url(<?php the_field("introduction__comment__image") ?>) ">        </div>
        <div class="introduction__comment__text col-sm-6 col-md-6 col-lg-6 col-xl-6">
            <h2><?php the_field("introduction__comment__text--sub-header") ?></h2>
            <p><?php the_field("introduction__comment__text") ?></p>
        </div>
    </div>

</div>

