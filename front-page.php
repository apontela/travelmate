<?php get_header() ?>

<div class="header" style="background-image: url(<?php echo get_field("hero_image_fp"); ?>)" >

    <?php get_template_part(navbar) ?>

    <div class="title-box">
        <h1><?php the_field("titulo_header"); ?></h1>

        <div class="button button--green button--header"><a href="#form">Empieza ahora</a></div>
    </div>
    
</div>

<?php get_template_part(introduction) ?>

<div class="purpose row">
    <div class="form-section offset-2 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <div id="form">
            <div class="form__head">
                <h3>Tus datos</h3>
            </div>
            <form class="form__data" action="">
                <div class="form__name row">
                    <label for="nombre" class="col-6">Nombre:</label>
                    <input type="text" name="nombre" id="nombre" class="col-6" required>
                </div>
                <div class="form__origin row">
                    <label for="origen" class="col-6">Ciudad de origen:</label>
                    <select name="origen" id="origen" class="col-6">
                        <option value="santiago">Santiago de Chile, Chile</option>
                        <option value="caracas">Caracas, Venezuela</option>
                        <option value="buenos+aires">Buenos Aires, Argentina</option>
                        <option value="lima">Lima, Perú</option>
                        <option value="washington">Washington DC, US</option>
                        <option value="madrid">Madrid, España</option>
                        <option value="medellin">Medellin, Colombia</option>
                        <option value="rio">Rio de Janeiro, Brasil</option>
                        <option value="ciudad+de+mexico">Ciudad de Mexico, Mexico</option>
                        <option value="toronto">Toronto, Canadá</option>
                    </select>                    
                </div>
                <div class="form__destination row">
                    <label for="destination" class="col-6">Ciudad de destino:</label>
                    <select name="destination" id="destination" class="col-6">
                        <option value="santiago">Santiago de Chile, Chile</option>
                        <option value="caracas" selected>Caracas, Venezuela</option>
                        <option value="buenos-aires">Buenos Aires, Argentina</option>
                        <option value="lima">Lima, Perú</option>
                        <option value="washington">Washington DC, US</option>
                        <option value="madrid">Madrid, España</option>
                        <option value="medellin">Medellin, Colombia</option>
                        <option value="rio">Rio de Janeiro, Brasil</option>
                        <option value="ciudad-de-mexico">Ciudad de Mexico, Mexico</option>
                        <option value="toronto">Toronto, Canadá</option>
                    </select>
                </div>
                <div class="center-button"><button id="form-submit" type="submit" class="button button--green button--form">Enviar</button></div>
            </form>
        </div>
    </div>
    <div class="plane-icon offset-1 col-sm-12 col-md-4 col-lg-4 col-xl-4">
        <img src="<?php the_field("plane-icon")?>" alt="plane-icon"></div>
</div>        
<div class="inspiration row">
    <div class="inspiration__text col-xs-8 col-sm-8 col-md-8 col-lg-6 col-xl-6"><p><?php the_field("inspiration__text") ?></p></div>
    <div class="inspiration__image col-xs-4 col-sm-4 col-md-4 col-lg-6 col-xl-6" style="background-image: url(<?php the_field("inspiration__image") ?>) "></div>
</div>

<?php get_template_part(herramientas) ?>

<footer id="footer" style="background-image: url(<?php the_field("fp__Footer-image") ?>)">
    <div class="footer__top col-xs-10 col-sm-10 col-md-10 col-lg-8 col-xl-8">
        <h1>¿Qué esperas?</h1>
        <div class="footer__button-holder">
            <form action="#form">
                <button class="button button--red button--footer">Empieza ahora</button>
            </form>
        </div>
        <p class="footer__motto"><?php the_field("footer__motto") ?></p>
    </div>
    <div class="footer__bottom row">
        <div class="personal-data col-xs-12 col-sm-10 col-md-6 col-lg-6 col-xl-6">
            <div class="personal-data__contact">
                <p class="personal-data__name"><?php the_field('personal-data__name');?></p>
                <p class="personal-data__number"><?php the_field("personal-data__number") ?></p>
                <p class="personal-data__email"><?php the_field("personal-data__email") ?></p>
            </div>
            <div class="personal-data__social">
                <span id="facebook"><a href=""><i class="fab fa-facebook-f"></i> Facebook    </a></span>
                <span id="twitter"><a href=""><i class="fab fa-twitter"></i> Twitter    </a></span>
                <span id="instagram"><a href=""><i class="fab fa-instagram"></i> Instagram    </a></span>
            </div>
        </div>
        <div class="footer__sponsored col-xs-12 col-sm-10 col-md-6 col-lg-6 col-xl-6">
            <p>Proyecto realizado para:</p>
            <img src="<?php the_field("sponsored-image") ?>" alt="">
        </div>
    </div>
</footer>

<?php get_sidebar() ?>

<?php get_footer() ?>