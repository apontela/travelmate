<?php

    /*
        Template Name: Colorful Gallery
    */

?>

<?php get_header() ?>
 
<div class="gallery-body2">
    <?php get_template_part(navbar) ?>
    <div class="gallery__title-box"><h1><?php the_title()?></h1></div>
    <div class="form-group">
        <label for="cities"><h3>Ciudad</h3></label>
        <select class="form-control" id="cities">
            <option value="santiago+de+chile">Santiago de Chile, Chile</option>
            <option value="caracas">Caracas, Venezuela</option>
            <option value="buenos+aires">Buenos Aires, Argentina</option>
            <option value="lima">Lima, Perú</option>
            <option value="washington">Washington DC, US</option>
            <option value="Madrid">Madrid, España</option>
            <option value="medellin">Medellin, Colombia</option>
            <option value="rio+de+janeiro" selected>Rio de Janeiro, Brasil</option>
            <option value="ciudad+de+mexico">Ciudad de Mexico, Mexico</option>
            <option value="toronto">Toronto, Canadá</option>
        </select>
    </div>
    <div class="gallery-grid2">
        <div><img id="img-0" src="" alt="img 0"></div>
        <div><img id="img-1" src="" alt="img 1"></div>
        <div><img id="img-2" src="" alt="img 2"></div>
        <div><img id="img-3" src="" alt="img 3"></div>
        <div><img id="img-4" src="" alt="img 4"></div>
        <div><img id="img-5" src="" alt="img 5"></div>
        <div><img id="img-6" src="" alt="img 6"></div>
        <div><img id="img-7" src="" alt="img 7"></div>
        <div><img id="img-8" src="" alt="img 8"></div>
        <div><img id="img-9" src="" alt="img 9"></div>
        <div><img id="img-10" src="" alt="img 10"></div>
        <div><img id="img-11" src="" alt="img 11"></div>
        <div><img id="img-12" src="" alt="img 12"></div>
        <div><img id="img-13" src="" alt="img 13"></div>
        <div><img id="img-14" src="" alt="img 14"></div>
        <div><img id="img-15" src="" alt="img 15"></div>
    </div>
    <p>Se muestran las fotos sobresalientes de la región seleccionada</p>
</div>

<?php get_sidebar() ?>
<?php get_footer() ?>