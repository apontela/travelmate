import $ from 'jquery';
window.$ = window.jQuery = $;
require ("chosen-js");
import swal from 'sweetalert';

$(document).ready( function($) {

	// Functions
	$("#origen").chosen();
	$("#destination").chosen();
	$(".button--header").click(function() {
		swal("¡Hola!", "Bienvenido a travelmate amigo, completa tus datos para empezar");
	})
	
});