<?php get_header() ?>


<div class="blog-header" style="background-image: url(http://localhost:8888/wordpress/wp-content/uploads/2018/10/joshua-earle-109629-unsplash.jpg)">
<?php get_template_part(navbar) ?>
    <h1>Algunas de las vivencias de nuestros autores:</h1>
</div>
<div class="blog-posts__grid">

<?php $args = array('post_type' => 'diario'); ?>
<?php $loop = new WP_Query($args); ?>
<?php if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); ?>

        <div>
        <a href="<?php the_permalink() ?>"><h2><?php the_title() ?></h2></a>
        <p><?php the_excerpt() ?></p>
        </div>

<?php endwhile; ?>

<?php else: ?>
    <h1>No posts here!</h1>
<?php endif; ?>
<?php wp_reset_postdata(); ?>

</ul>

</div>    
<?php get_sidebar() ?>
<?php get_footer() ?>