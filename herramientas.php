<div class="herramientas" id="herramientas">
    <div class="herramientas__head"><h2><?php the_field("herramientas__head") ?></h2></div>
    <div class="row">
        <div class="gallery-card col-xs-10 col-sm-10 col-md-6 col-lg-4 col-xl-4">
            <div class="card">
                <div class="card__image"><img src="<?php the_field("gallery-card__image") ?>" alt="gallery icon"></div>
                <div class="card__text"><p><?php the_field("gallery-card__text") ?></p></div>
            </div>
            <form action="http://localhost:8888/wordpress/gallery/">
                <button class="button button--dark-green button--card">
                    Go
                </button>
            </form>
            
        </div>
        <div class="wheather-card col-xs-10 col-sm-10 col-md-6 col-lg-4 col-xl-4">
            <div class="card">
                <div class="card__image"><img src="<?php the_field("wheather-card__image") ?>" alt="wheather icon"></div>
                <div class="card__text"><p><?php the_field("wheather-card__text") ?></p></div>
            </div>
            <form action="http://localhost:8888/wordpress/clima/">
                <button class="button button--dark-green button--card">
                    Go
                </button>
            </form>
        </div>
        <div class="trending-card col-xs-10 col-sm-10 col-md-6 col-lg-4 col-xl-4">
            <div class="card">
                <div class="card__image"><img src="<?php the_field("trending-card__image") ?>" alt="trending icon"></div>
                <div class="card__text"><p><?php the_field("trending-card__text") ?></p></div>
            </div>
            <form action="http://localhost:8888/wordpress/maps/">
                <button class="button button--dark-green button--card">
                    Go
                </button>
            </form>
        </div>
        <div class="money-card col-xs-10 col-sm-10 col-md-6 col-lg-4 col-xl-4">
            <div class="card">
                <div class="card__image"><img src="<?php the_field("money-card__image") ?>" alt="money icon"></div>
                <div class="card__text"><p><?php the_field("money-card__text") ?></p></div>
            </div>
            <form action="http://localhost:8888/wordpress/divisas/">
                <button class="button button--dark-green button--card">
                    Go
                </button>
            </form>
        </div>
        <div class="housing-card col-xs-10 col-sm-10 col-md-6 col-lg-4 col-xl-4">
            <div class="card">
                <div class="card__image"><img src="<?php the_field("housing-card__image") ?>" alt="housing icon"></div>
                <div class="card__text"><p><?php the_field("housing-card__text") ?></p></div>
            </div>
            <form action="http://localhost:8888/wordpress/alojamiento/">
                <button class="button button--dark-green button--card">
                    Go
                </button>
            </form>
        </div>
        <div class="diary-card col-xs-10 col-sm-10 col-md-6 col-lg-4 col-xl-4">
            <div class="card">
                <div class="card__image"><img src="<?php the_field("diary-card__image") ?>" alt="diary icon"></div>
                <div class="card__text"><p><?php the_field("diary-card__text") ?></p></div>
            </div>
            <form action="http://localhost:8888/wordpress/diario/">
                <button class="button button--dark-green button--card">
                    Go
                </button>
            </form>
        </div>
    </div>

</div>