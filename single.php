<?php get_header() ?>
<?php get_template_part(navbar) ?>

<div class="container single-post">
<?php if ( have_posts() ) { ?>
	<?php while ( have_posts() ) { ?>
		<?php the_post(); ?>
		
		<div class="post">
			<div class="post-head">
				<h1><?php the_title() ?></h1>
				<time datetime="<?php the_time('Y-m-d') ?>"><?php the_time('d \d\e F \d\e Y') ?></time>
			</div>
			<div class="post-body">
				<div><img class="img-fluid" src="<?php the_post_thumbnail_url('medium_large') ?>"></div>
				<div><?php the_content() ?></div>

			</div>
			
		</div>


	<?php } ?>
<?php } else { ?>
	<!-- Content -->
<?php } wp_reset_query(); ?>
</div>

<?php get_sidebar() ?>
<?php get_footer() ?>