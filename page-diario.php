<?php get_header() ?>


<div class="blog-header" style="background-image: url(<?php the_post_thumbnail_url() ?>)">
<?php get_template_part(navbar) ?>
    <h1><?php the_field('introduccion_de_diario') ?></h1>
</div>
<div class="blog-posts__grid">

<?php $args = array('post_type' => 'diario'); ?>
<?php $loop = new WP_Query($args); ?>
<?php if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); ?>

        <div>
        <a href="<?php the_permalink() ?>"><h2><?php the_title() ?></h2></a>
        <p><?php the_excerpt() ?></p>
        </div>

<?php endwhile; ?>

<?php else: ?>
    <h1>No posts here!</h1>
<?php endif; ?>
<?php wp_reset_postdata(); ?>

</ul>

</div>    
<?php get_sidebar() ?>
<?php get_footer() ?>
